// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka


// XFW.XInput
#include "wot_avatar.h"

namespace XFW::XInput {
    //
    // Init
    //

    WoTAvatar::WoTAvatar()
    {
        pybind11::gil_scoped_acquire acquire;
        _module_bigworld = pybind11::module::import("BigWorld");
    }

    void WoTAvatar::ProcessInput(BigworldKey moveCommand, CruiseControlMode cruiseMode, float dx, float dy, int dz)
    {
        pybind11::gil_scoped_acquire acquire;
        if (moveCommand != _moveCommand_prev) {
            sendMoveCommand(moveCommand);
            _moveCommand_prev = moveCommand;
        }

        if (cruiseMode != _cruiseMode_prev)
        {
            updateCruiseControlUi(cruiseMode);
            _cruiseMode_prev = cruiseMode;
        }

        sendMouse(dx, dy, dz);
    }


    //
    // Private
    //

    bool WoTAvatar::sendMoveCommand(BigworldKey command)
    {
        auto playerAvatarCell = getPlayerAvatarCell();
        if (!playerAvatarCell) {
            return false;
        }

        playerAvatarCell.attr("vehicle_moveWith")(static_cast<uint8_t>(command));
        return true;
    }

    bool WoTAvatar::sendMouse(float dx, float dy, int dz)
    {
        auto avatarController = getPlayerAvatarInputHandlerCurrentController();
        if (!avatarController) {
            return false;
        }

        avatarController.attr("handleMouseEvent")(dx, dy, dz);
        return true;
    }

    bool WoTAvatar::updateCruiseControlUi(CruiseControlMode mode)
    {
        auto guiSessionProvider = getPlayerAvatarGuiSessionProvider();
        if (!guiSessionProvider) {
            return false;
        }

        guiSessionProvider.attr("invalidateVehicleState")(static_cast<int>(VehicleViewState::CRUISE_MODE), static_cast<int>(mode));
        return true;
    }

    pybind11::object WoTAvatar::getPlayerAvatar()
    {
        if (!_module_bigworld)
        {
            return {};
        }

        auto playerAvatar = _module_bigworld.attr("player")();
        if (!playerAvatar) {
            return {};
        }


        if (!pybind11::hasattr(playerAvatar, "_PlayerAvatar__isOnArena")) {
            return {};
        }

        if (!pybind11::bool_(playerAvatar.attr("_PlayerAvatar__isOnArena"))){
            return {};
        }

        return playerAvatar;
    }

    pybind11::object WoTAvatar::getPlayerAvatarBase()
    {
        auto playerAvatar = getPlayerAvatar();
        if (!playerAvatar) {
            return {};
        }

        if (!pybind11::hasattr(playerAvatar, "base")) {
            return {};
        }

        auto playerAvatarBase = playerAvatar.attr("base");
        if (!playerAvatarBase) {
            return {};
        }

        return playerAvatarBase;
    }

    pybind11::object WoTAvatar::getPlayerAvatarCell()
    {
        auto playerAvatar = getPlayerAvatar();
        if (!playerAvatar) {
            return {};
        }

        if (!pybind11::hasattr(playerAvatar, "cell")) {
            return {};
        }

        auto playerAvatarCell = playerAvatar.attr("cell");
        if (!playerAvatarCell) {
            return {};
        }

        return playerAvatarCell;
    }

    pybind11::object WoTAvatar::getPlayerAvatarInputHandler()
    {
        auto playerAvatar = getPlayerAvatar();
        if (!playerAvatar) {
            return {};
        }

        if (!pybind11::hasattr(playerAvatar, "inputHandler")) {
            return {};
        }

        auto inputHandler = playerAvatar.attr("inputHandler");
        if (!inputHandler) {
            return {};
        }

        return inputHandler;
    }

    pybind11::object WoTAvatar::getPlayerAvatarInputHandlerCurrentController()
    {
        auto inputHandler = getPlayerAvatarInputHandler();
        if (!inputHandler) {
            return {};
        }

        if (!pybind11::hasattr(inputHandler, "_AvatarInputHandler__curCtrl")) {
            return {};
        }

        auto currentController = inputHandler.attr("_AvatarInputHandler__curCtrl");
        if (!currentController) {
            return {};
        }

        return currentController;
    }

    pybind11::object WoTAvatar::getPlayerAvatarGuiSessionProvider()
    {
        auto playerAvatar = getPlayerAvatar();
        if (!playerAvatar) {
            return {};
        }

        if (!pybind11::hasattr(playerAvatar, "guiSessionProvider")) {
            return {};
        }

        auto guiSessionProvider = playerAvatar.attr("guiSessionProvider");
        if (!guiSessionProvider) {
            return {};
        }

        return guiSessionProvider;
    }

}
