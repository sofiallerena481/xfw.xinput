// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>

// pybind11
#include <pybind11/pybind11.h>

// XFW.XInput
#include "constants.h"


namespace XFW::XInput {
    class WoTAvatar
    {
    public:
        WoTAvatar();

        void ProcessInput(BigworldKey moveCommand, CruiseControlMode cruiseMode, float dx, float dy, int dz);

    private:
        bool sendMoveCommand(BigworldKey command);
        bool sendMouse(float dx, float dy, int dz);
        bool updateCruiseControlUi(CruiseControlMode mode);

        pybind11::object getPlayerAvatar();
        pybind11::object getPlayerAvatarBase();
        pybind11::object getPlayerAvatarCell();
        pybind11::object getPlayerAvatarInputHandler();
        pybind11::object getPlayerAvatarInputHandlerCurrentController();
        pybind11::object getPlayerAvatarGuiSessionProvider();

    private:
        pybind11::module _module_bigworld;

        BigworldKey _moveCommand_prev{ BigworldKey::BW_NONE };
        CruiseControlMode _cruiseMode_prev{ CruiseControlMode::NONE };
    };
}