// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright (c) 2018-2022 Mikhail Paulyshka

#pragma once

#include <cstdint>
#include <type_traits>

namespace XFW::XInput {
	enum class XInputKey : uint16_t
	{
		XB_NONE = 0,

		XB_DPAD_UP = 1 << 0,
		XB_DPAD_DOWN = 1 << 1,
		XB_DPAD_LEFT = 1 << 2,
		XB_DPAD_RIGHT = 1 << 3,
		XB_START = 1 << 4,
		XB_BACK = 1 << 5,
		XB_LEFT_THUMB = 1 << 6,
		XB_RIGHT_THUMB = 1 << 7,
		XB_LEFT_SHOULDER = 1 << 8,
		XB_RIGHT_SHOULDER = 1 << 9,
		XB_A = 1 << 10,
		XB_B = 1 << 11,
		XB_X = 1 << 12,
		XB_Y = 1 << 13,
	};


	enum class BigworldKey : uint8_t {
		BW_NONE = 0,

		BW_FORWARD = 1 << 0,
		BW_BACKWARD = 1 << 1,
		BW_ROTATE_LEFT = 1 << 2,
		BW_ROTATE_RIGHT = 1 << 3,
		BW_CRUISE_CONTROL50 = 1 << 4,
		BW_CRUISE_CONTROL25 = 1 << 5,
		BW_BLOCK_TRACKS = 1 << 6,
	};

	enum class CruiseControlMode : int8_t
	{
		NONE = 0,
		FWD25 = 1,
		FWD50 = 2,
		FWD100 = 3,
		BCKW50 = -1,
		BCKW100 = -2,
	};


	enum class VehicleViewState : uint32_t {
		FIRE = 1 << 0,
		DEVICES = 1 << 1,
		HEALTH = 1 << 2,
		DESTROYED = 1 << 3,
		CREW_DEACTIVATED = 1 << 4,
		AUTO_ROTATION = 1 << 5,
		SPEED = 1 << 6,
		CRUISE_MODE = 1 << 7,
		REPAIRING = 1 << 8,
		PLAYER_INFO = 1 << 9,
		DESTROY_TIMER = 1 << 10,
		OBSERVED_BY_ENEMY = 1 << 12,
		RESPAWNING = 1 << 13,
		SWITCHING = 1 << 14,
		DEATHZONE_TIMER = 1 << 15,
		DEATH_INFO = 1 << 17,
		VEHICLE_CHANGED = 1 << 18,
		SIEGE_MODE = 1 << 19,
		STUN = 1 << 24,
		CAPTURE_BLOCKED = 1 << 25,
		SMOKE = 1 << 26,
		INSPIRE = 1 << 27,
		UNDER_FIRE = 1 << 28,
		RECOVERY = 1 << 29,
		PROGRESS_CIRCLE = 1 << 30,
		CLIENT_ONLY = AUTO_ROTATION | CRUISE_MODE,
	};


	inline BigworldKey operator|(BigworldKey lhs, BigworldKey rhs)
	{
		return static_cast<BigworldKey>(static_cast<std::underlying_type<BigworldKey>::type>(lhs) | static_cast<std::underlying_type<BigworldKey>::type>(rhs));
	}

	inline BigworldKey& operator|=(BigworldKey& lhs, BigworldKey rhs)
	{
		return lhs = lhs | rhs;
	}
}